<?php namespace Tekton\API\Facades;

class API extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'api'; }
}
